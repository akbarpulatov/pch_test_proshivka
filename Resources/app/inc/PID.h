/********************************************************************************
 * project		Tester For PCH_450V PCB											*
 *				                                                                *
 * file			PID.h															*
 * author		Akbar Pulatov													*
 * date																			*
 * copyright	Akbar Pulatov(C)												*
 * brief																		*
 *                                                                              *
 ********************************************************************************/

/********************************************************************************
 * Include 
 ********************************************************************************/
#pragma once 

#include "stm32f4xx.h"

/********************************************************************************
 * Define
 ********************************************************************************/   



/********************************************************************************
 * User enum
 ********************************************************************************/

/********************************************************************************
 * User typedef
 ********************************************************************************/

/********************************************************************************
 * Local function declaration
 ********************************************************************************/
uint16_t CalculPIDforPWM(uint16_t TempU_Max);


/********************************* END OF FILE **********************************/
