/********************************************************************************
 * project																		*
 *                                                                              *
 * file			PID.c															*
 * author		Akbar Pulatov													*
 * date			09.08.2019                                                      *
 * copyright	Akbar Pulatov(C)												*
 * brief																		*
 *                                                                              *
 ********************************************************************************/

/********************************************************************************
 * Include 
 ********************************************************************************/

#include "PID.h"
#include "PwmForBoostConverter.h"

/********************************************************************************
 * Used variable
 ********************************************************************************/

const float _kp = 2.0;
const float	_ki = 0.0005;
const float	_kd = 2.0;

float Error, LastError, Integral, Derivative;
float ActualUNet, SetUNet;
int PWM = 0;
extern float U_Net;

/********************************************************************************
 * PID
 ********************************************************************************/

uint16_t CalculPIDforPWM(uint16_t TempU_Max)
{
	int temp;
	SetUNet = TempU_Max;
	ActualUNet = U_Net;

	Error = SetUNet - ActualUNet;	
	Integral = Integral + Error;	
	Derivative = Error - LastError;	
	
	PWM = (_kp*Error) + (_ki*Integral) + (_kd*Derivative);
	
	if (PWM > 0.03 * PeriodTimer) 
	{
		PWM = 0.03 * PeriodTimer;
	}
	else if (PWM < 0)		
	{
		PWM = 0;
	}
	
	LastError = Error;

	if (Error >= 100)
	{
		Integral = 0;
	}
	else if (Error <= -100) 
	{
		Integral = 0;
	}
	;
	
	return PWM;
}

/********************************* END OF FILE **********************************/