/********************************************************************************
 * project																		*
 *																				*
 * file			main.c															*
 * author		Akbar Pulatov													*
 * date																			*
 * copyright	Akbar Pulatov(C)												*
 * brief																		*
 *																				*
 ********************************************************************************/

/********************************************************************************
 * Include 
 ********************************************************************************/

#include "main.h"

/********************************************************************************
 * Used variable
 ********************************************************************************/

uint32_t TestPwm = 0;
uint16_t watchtemp = 0;
uint16_t watchPWM = 0;
uint16_t watchFreq = 0;
extern float U_Net;

/********************************************************************************
 * Main Function
 ********************************************************************************/

int main(void)
{
	
	
	
	InitSystemClockMCU();
	InitGpioForDebug();
	InitAdcForFeedback();
	InitTimerForAdc(100);
	InitPWMForBoostConverter(4);
	
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOCEN;

	//PC13
	GPIOC->MODER   &= ~GPIO_MODER_MODE13;
	GPIOC->OTYPER  &= ~GPIO_OTYPER_OT13;
	GPIOC->OSPEEDR &= ~GPIO_OSPEEDR_OSPEED13;
	
	GPIOC->MODER |=   0b01 << GPIO_MODER_MODE13_Pos;
	GPIOC->OTYPER |=  0b00 << GPIO_OTYPER_OT13_Pos;
	GPIOC->OSPEEDR |= 0b11 << GPIO_OSPEEDR_OSPEED13_Pos;
	
	PWMout1(PeriodTimer * 0.03); 
	watchtemp = 10;
	watchFreq = 1;
		while(1)
	{
		Delay();
//		watchtemp = CalculPIDforPWM(25);
////		PWMout1(watchtemp);
//		
//		if (U_Net > 5)
//		{
			TIM8->PSC = watchFreq;      							//Prescaler		 
			
			PWMout1(watchtemp);
			PWMout2(watchtemp);
			PWMout3(watchtemp);
		
		
//		}
//		else
//		{
//			PWMout1(0);
//			PWMout2(0);
//			PWMout3(0);
//		}
		
//		PWMout1(  (TestPwm) % PeriodTimer);
//		PWMout2(  (TestPwm) % PeriodTimer);
//		PWMout3((TestPwm++) % PeriodTimer);
	}
}

/********************************************************************************
 * Dummy Delay Function
 ********************************************************************************/

void Delay(void)
{
	int i;
	for (i = 0; i < 20; i++)
		asm("nop");
}

/********************************* END OF FILE **********************************/