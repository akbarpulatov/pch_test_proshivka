/********************************************************************************
 * project																		*
 *																				*
 * file			PwmForBoostConverter.c											*
 * author		Akbar Pulatov													*
 * date																			*
 * copyright	Akbar Pulatov(C)												*
 * brief																		*
 *																				*
 ********************************************************************************/

/********************************************************************************
 * Include 
 ********************************************************************************/

#include "mydeff.h"
#include "PwmForBoostConverter.h"

/********************************************************************************
 * Used variable
 ********************************************************************************/


/********************************************************************************
 * Disable output gpio for PWM
 * Set all gpio in logic 0 set logic 1
 * Setting in logic 1 disabling output transistor
 ********************************************************************************/

void StopAllConverter(void) 
{
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN | RCC_AHB1ENR_GPIOBEN;

	GPIOA->MODER |= GPIO_MODER_MODER8_0;
	GPIOA->MODER |= GPIO_MODER_MODER9_0;
	GPIOA->MODER |= GPIO_MODER_MODER10_0;
	GPIOA->MODER |= GPIO_MODER_MODER11_0;

	GPIOB->MODER |= GPIO_MODER_MODER12_0;
	GPIOB->MODER |= GPIO_MODER_MODER13_0;
	GPIOB->MODER |= GPIO_MODER_MODER14_0;
	GPIOB->MODER |= GPIO_MODER_MODER15_0;

	GPIOA->BSRR |= GPIO_BSRR_BR_8;
	GPIOA->BSRR |= GPIO_BSRR_BR_9;
	GPIOA->BSRR |= GPIO_BSRR_BR_10;
	GPIOA->BSRR |= GPIO_BSRR_BR_11;

	GPIOB->BSRR |= GPIO_BSRR_BR_12;
	GPIOB->BSRR |= GPIO_BSRR_BR_13;
	GPIOB->BSRR |= GPIO_BSRR_BS_14;
	GPIOB->BSRR |= GPIO_BSRR_BR_15;
}

/********************************************************************************
 * Initialization GPIO for HRPWM channel B
 * GPIOs used - PC6, PC7, PC8 - Alternate Function Push Pull, Very High Speed
 * GPIO Multiplexer for Timer 8 - AF3
 ********************************************************************************/

void InitGpioForPWM(void)
{
	RCC->AHB1ENR  |= RCC_AHB1ENR_GPIOCEN;                // Enable clock for GPIO port C

	//PC6
	GPIOC->MODER   &= ~GPIO_MODER_MODE6;
	GPIOC->OTYPER  &= ~GPIO_OTYPER_OT6;
	GPIOC->OSPEEDR &= ~GPIO_OSPEEDR_OSPEED6;
	
	GPIOC->MODER   |= 0b10 << GPIO_MODER_MODE6_Pos;
	GPIOC->OTYPER  |= 0b00 << GPIO_OTYPER_OT6_Pos;
	GPIOC->OSPEEDR |= 0b11 << GPIO_OSPEEDR_OSPEED6_Pos;
	
	//PC7
	GPIOC->MODER   &= ~GPIO_MODER_MODE7;
	GPIOC->OTYPER  &= ~GPIO_OTYPER_OT7;
	GPIOC->OSPEEDR &= ~GPIO_OSPEEDR_OSPEED7;
	
	GPIOC->MODER   |= 0b10 << GPIO_MODER_MODE7_Pos;
	GPIOC->OTYPER  |= 0b00 << GPIO_OTYPER_OT7_Pos;
	GPIOC->OSPEEDR |= 0b11 << GPIO_OSPEEDR_OSPEED7_Pos;
	
	//PC8
	GPIOC->MODER   &= ~GPIO_MODER_MODE8;
	GPIOC->OTYPER  &= ~GPIO_OTYPER_OT8;
	GPIOC->OSPEEDR &= ~GPIO_OSPEEDR_OSPEED8;
	
	GPIOC->MODER   |= 0b10 << GPIO_MODER_MODE8_Pos;
	GPIOC->OTYPER  |= 0b00 << GPIO_OTYPER_OT8_Pos;
	GPIOC->OSPEEDR |= 0b11 << GPIO_OSPEEDR_OSPEED8_Pos;

	
	//GPIO Multiplexer - AF3 is for Timer8 - pg.272
	GPIOC->AFR[0] &= ~GPIO_AFRL_AFSEL7;
	GPIOC->AFR[0] |= 3 << GPIO_AFRL_AFSEL6_Pos;	
	
	GPIOC->AFR[0] &= ~GPIO_AFRL_AFSEL7;
	GPIOC->AFR[0] |= 3 << GPIO_AFRL_AFSEL7_Pos; 
		
	GPIOC->AFR[1] &= ~GPIO_AFRH_AFSEL8;
	GPIOC->AFR[1] |= 3 << GPIO_AFRH_AFSEL8_Pos; 
}

/********************************************************************************
 * PWM Initialization Function
 * ARR - 1000
 * Channel 1 - PC6
 * Channel 2 - PC7
 * Channel 3 - PC8
 ********************************************************************************/

void InitPWMForBoostConverter(uint8_t kilohertz)
{
	InitGpioForPWM();
	
	RCC->APB2ENR |= RCC_APB2ENR_TIM8EN;								//Enable Peripheral	
	TIM8->CCER = 0;													//Reset CCER(Turn All channels OFF)	
	
	TIM8->CCR1 = 0;													//Reset Duty Cycles
	TIM8->CCR2 = 0;													//Reset Duty Cycles
	TIM8->CCR3 = 0;													//Reset Duty Cycles
	
	TIM8->PSC = (4 - 1) / kilohertz;    							//Prescaler		 
//	TIM8->PSC = (168 - 1) / (float)(PeriodTimer / 1000.00) / (float)kilohertz;   	//Prescaler		 
	TIM8->ARR = PeriodTimer - 1; 									//Period		 
	TIM8->BDTR |= TIM_BDTR_MOE;										//Main output enable
	
	TIM8->CCMR1 = 0b110 << TIM_CCMR1_OC1M_Pos						//Channel 1 Not Inverter Output
				| 0b110 << TIM_CCMR1_OC2M_Pos;						//Channel 2 Not Inverter Output
	
	TIM8->CCMR2 = 0b110 << TIM_CCMR2_OC3M_Pos;						//Channel 3 Not Inverter Output
	
	TIM8->CCER |= TIM_CCER_CC1E | TIM_CCER_CC2E | TIM_CCER_CC3E;	//Capture/Compare 4 output enable
	TIM8->CR1 |= TIM_CR1_CEN;										 //Start Timer
}

/********************************************************************************
 * Select and enable generation event for ADC
 ********************************************************************************/

void SelectEventForExternalGeneration(void) 
{
//	HRTIM1->sTimerxRegs[1].CMP2xR = PeriodTimerB / 10;   // Samples in 10% of ON time 
//	HRTIM1->sCommonRegs.CR1 |= HRTIM_CR1_ADC2USRC_1;     // ADC trigger 2 update: Timer B 
//	HRTIM1->sCommonRegs.ADC2R |= HRTIM_ADC2R_AD2TBC2;    // ADC trigger 2 event: Timer B compare 2  
}

/********************************************************************************
 * Set value duty for HRPWM channel B
 ********************************************************************************/

void SetDutyTimerB(uint16_t duty) 
{ 
    
//	HRTIM1->sTimerxRegs[1].CMP1xR = PeriodTimerB - duty; 
}

/********************************* END OF FILE **********************************/