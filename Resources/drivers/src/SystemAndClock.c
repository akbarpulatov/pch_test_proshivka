/********************************************************************************
 * project																		*
 *																				*
 * file			SystemAndClock.c												*
 * author		Akbar Pulatov													*
 * date																			*
 * copyright	Akbar Pulatov(C)												*
 * brief																		*
 *																				*
 ********************************************************************************/

/********************************************************************************
 * Include 
 ********************************************************************************/

#include "SystemAndClock.h"

/********************************************************************************
 * Used variable
 ********************************************************************************/



/********************************************************************************
 * Initialization for the clock's system
 * Source - external crystall
 * Frequency external crystal   - 8 000 000 Hz
 * Frequency for system clock   - 168 000 000 Hz
 * M = 15, N = 192, P = 15
 * Divider for ADC clock system - 
 ********************************************************************************/

void InitSystemClockMCU(void)
{
	//Latency for internal flash memory - 5 WS - pg.80
	//Flash Latency corresponding to 168MHz SysClk
	FLASH->ACR |= (5 << FLASH_ACR_LATENCY_Pos);
	
	//Turn on HSE and wait until ready
	RCC->CR |= RCC_CR_HSEON;
	while (!(RCC->CR & RCC_CR_HSERDY)) ;
	
	
	RCC->PLLCFGR = 0x24003010;														//Reset Value for PLLCFGR
	//Config PLL
	RCC->PLLCFGR |= RCC_PLLCFGR_PLLSRC_HSE;											//PLL source HSE
	
	RCC->PLLCFGR &= ~(RCC_PLLCFGR_PLLM | RCC_PLLCFGR_PLLN | RCC_PLLCFGR_PLLP);		//Reset Values
	RCC->PLLCFGR |=    4 << RCC_PLLCFGR_PLLM_Pos									//Devision Factor M = 4
				 |   168 << RCC_PLLCFGR_PLLN_Pos									//Multipli Factor N = 168
				 |  0b00 << RCC_PLLCFGR_PLLP_Pos									//Devision Factor P = 2 
				 | 	   7 << RCC_PLLCFGR_PLLQ_Pos;									//Devision Factor Q = 7
	
	RCC->CR |= RCC_CR_CSSON;
		
	//Turn on PLL and wait until ready
	//RCC->CFGR |= (0b0111 << RCC_CFGR_PLLMULL_Pos) | RCC_CFGR_PLLSRC;
		
	RCC->CR |= RCC_CR_PLLON;
	while (!(RCC->CR & RCC_CR_PLLRDY)) {}
		
	//AHB prescaler = NOT divided, APB2 prescaler = 2 , APB1 prescalers prescaler = 4
	RCC->CFGR |= 0b0000 << RCC_CFGR_HPRE_Pos
			  |   0b100 << RCC_CFGR_PPRE2_Pos
			  |   0b101 << RCC_CFGR_PPRE1_Pos;
	
	
	RCC->CFGR |= RCC_CFGR_SW_PLL;
	while ((RCC->CFGR & RCC_CFGR_SWS_PLL) != RCC_CFGR_SWS_PLL) ;
	
	EnableOutputMCO1();
	EnableOutputMCO2();
}

/********************************************************************************
 * Initialization MCO output
 * MCO2 - PC9
 * MCO2 Source - SysClk
 * MCO2 Prescaler - 5
 ********************************************************************************/

void EnableOutputMCO2(void)
{
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOCEN;
	
	//PC9 = MCO2
	GPIOC->MODER &= ~GPIO_MODER_MODE9;
	GPIOC->OTYPER &= ~GPIO_OTYPER_OT9;
	GPIOC->OSPEEDR &= ~GPIO_OSPEEDR_OSPEED9;
	
	GPIOC->MODER |=   0b10 << GPIO_MODER_MODE9_Pos;
	GPIOC->OTYPER |=  0b00 << GPIO_OTYPER_OT9_Pos;
	GPIOC->OSPEEDR |= 0b11 << GPIO_OSPEEDR_OSPEED9_Pos;
	
	//MCO2 prescaler - 5
	RCC->CFGR &= ~RCC_CFGR_MCO2PRE;
	RCC->CFGR |= 0b111 << RCC_CFGR_MCO2PRE_Pos;
	//SysClk as MCO
	RCC->CFGR &= ~RCC_CFGR_MCO2;
	RCC->CFGR |= 0b00 << RCC_CFGR_MCO2_Pos;
}

/********************************************************************************
 * Initialization MCO output
 * MCO1 - PA8
 * MCO1 Source - 
 * MCO1 Prescaler - 5
 ********************************************************************************/

void EnableOutputMCO1(void)
{
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;
	
	//PA8 = MCO1
	GPIOA->MODER &= ~GPIO_MODER_MODE8;
	GPIOA->OTYPER &= ~GPIO_OTYPER_OT8;
	GPIOA->OSPEEDR &= ~GPIO_OSPEEDR_OSPEED8;
	
	GPIOA->MODER |=   0b10 << GPIO_MODER_MODE8_Pos;
	GPIOA->OTYPER |=  0b00 << GPIO_OTYPER_OT8_Pos;
	GPIOA->OSPEEDR |= 0b11 << GPIO_OSPEEDR_OSPEED8_Pos;
	
	//MCO1 prescaler - 
	RCC->CFGR &= ~RCC_CFGR_MCO1PRE;
	RCC->CFGR |= 0b111 << RCC_CFGR_MCO1PRE_Pos;
	//HSE as MCO
	RCC->CFGR &= ~RCC_CFGR_MCO1;
	RCC->CFGR |= 0b10 << RCC_CFGR_MCO1_Pos;
}

/********************************* END OF FILE **********************************/