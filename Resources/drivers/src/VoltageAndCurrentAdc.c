/********************************************************************************
 * project																		*
 *                                                                              *
 * file        ADC.c															*
 * author      Akbar Pulatov													*
 * date        13.02.2020                                                       *
 * copyright   Akbar Pulatov(C)													*
 * brief       Work with ADC for measuring voltage								*
 *                                                                              *
 ********************************************************************************/

/********************************************************************************
 * Include 
 ********************************************************************************/

#include "VoltageAndCurrentAdc.h"
#include "PID.h"
#include "PwmForBoostConverter.h"

/********************************************************************************
 * Used variable
 ********************************************************************************/
float resultCurrentOutput = 0;
float resultVoltageOutput = 0;
float resultCurrentInput = 0;
float resultVoltageInput = 0;
float U_Net;

uint8_t stepFilterForAdc = 0;
const uint8_t allStepFilterAdc = 40;

float bufferCurrentOutput = 0;
float bufferVoltageOutput = 0;
float bufferCurrentInput = 0;
float bufferVoltageInput = 0;

const float referenceVoltageForAdc = 0.000807;				// Step = VDDA / 4096

const float offsetForOutputCurrentSensor = 0.516;			// Voltage offset 10% * VCC sensor
const float offsetForInputCurrentSensor = 0.52;				// Voltage offset 10% * VCC sensor
const float SensitivityCurrentSensor = 0.133;				// Sensitivity sensor 133 mV/A

const float dividerResistorCurrentOutput = 1.56;			// Divider = (5.6 kOhm / 10 kOhm) + 1 
const float dividerResistorVoltageOutput = 61.6060;			// Divider = (200 kOhm / 3.3 kOhm) + 1
const float dividerResistorCurrentInput = 1.56;				// Divider = (5.6 kOhm / 10 kOhm) + 1
const float deviderResistorVoltageInput = 18.8571;			// Divider = (100 kOhm / 5.6 kOhm) + 1

uint16_t tempwatchadc1 = 0;
uint16_t tempwatchadc2 = 0;
uint16_t tempwatchadc3 = 0;
uint16_t tempwatchadc4 = 0;

/********************************************************************************
 * Initiallization ADC3
 ********************************************************************************/

void InitAdcForFeedback(void)
{
	RCC->APB2ENR |=  RCC_APB2ENR_ADC3EN; 		//ADC3 Enable
	
	InitGpioForFeedbackAdc();
	StartCallibrationForAdc();
	
	ADC3->CR1 = 0;								//Presetting the Register
	ADC3->CR1 =  0b00 << ADC_CR1_RES_Pos		//12 bit resolution
			  |     0 << ADC_CR1_JAUTO_Pos		//Automatic injected group conversion
			  |     1 << ADC_CR1_SCAN_Pos		//Scan mode
			  |     1 << ADC_CR1_JEOCIE_Pos;	//Interrupt enable for injected channels
	
	ADC3->CR2 = 0;
	ADC3->CR2 |=   0b00 << ADC_CR2_JEXTEN_Pos	//
			  |  0b0000 << ADC_CR2_JEXTSEL_Pos	//
			  |		  0 << ADC_CR2_ALIGN_Pos	//Right alignment
			  |		  0 << ADC_CR2_EOCS_Pos;	//End of conversion is the end of sequence
		
	ADC3->JSQR = (4 - 1) << ADC_JSQR_JL_Pos		//Number of Channels
			   | 4 << ADC_JSQR_JSQ1_Pos			//1 - conversion
			   | 4 << ADC_JSQR_JSQ2_Pos			//2 - conversion
			   | 6 << ADC_JSQR_JSQ3_Pos			//3 - conversion
			   | 7 << ADC_JSQR_JSQ4_Pos; 		//4 - conversion
	
	//Sample Time/rate Selection
	ADC3->SMPR2 = 0;
	ADC3->SMPR2 |= 0b111 << ADC_SMPR2_SMP4_Pos
				|  0b111 << ADC_SMPR2_SMP6_Pos
				|  0b111 << ADC_SMPR2_SMP7_Pos;
	
	ADC3->CR2    |=  ADC_CR2_ADON;					//ADC ON
	
	NVIC_EnableIRQ(ADC_IRQn);						//Enable Interrupt
}

/********************************************************************************
 * Initialization GPIO for ADC3
 * Voltage output(450V) - ADC3 channel 4 - PF6
 * Voltage input(110V)  - ADC3 channel 6 - PF8
 * Current input		- ADC3 channel 7 - PF9
 ********************************************************************************/

void InitGpioForFeedbackAdc(void) 
{
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOFEN;		// Clock enable for GPIO port F
	
	//PF6
	GPIOF->MODER &= ~GPIO_MODER_MODE4;
	GPIOF->MODER |=   0b11 << GPIO_MODER_MODE6_Pos;
	
	//PF8
	GPIOF->MODER &= ~GPIO_MODER_MODE8;
	GPIOF->MODER |=   0b11 << GPIO_MODER_MODE8_Pos;
	
	//PF9
	GPIOF->MODER &= ~GPIO_MODER_MODE9;
	GPIOF->MODER |=   0b11 << GPIO_MODER_MODE9_Pos;
}

/********************************************************************************
 * Callibration for ADC
 ********************************************************************************/

void StartCallibrationForAdc(void) 
{
//	ADC123_COMMON->CCR &= ~ADC_CCR_TSVREFE;
//	ADC123_COMMON->CCR |= ADC_CCR_TSVREFE;   		// Enable Vref
//	ADC123_COMMON->CCR &= ~ADC_CR_ADCALDIF;
//	   
//	ADC123_COMMON->CCR |= ADC_CR_ADCAL;             // Start calibration
//	while(ADC123_COMMON->CCR & ADC_CR_ADCAL);      // Wait end calibration
}

/********************************************************************************
 * ADC Interrupt Handler
 ********************************************************************************/

void ADC_IRQHandler(void)
{
//	asm("BKPT");
	ADC3->SR &= ~ADC_SR_JEOC;
	
	tempwatchadc1 = ADC3->JDR1;
	tempwatchadc2 = ADC3->JDR2;
	tempwatchadc3 = ADC3->JDR3;
	tempwatchadc4 = ADC3->JDR4;
	
	bufferVoltageOutput += dividerResistorVoltageOutput * referenceVoltageForAdc * (ADC3->JDR1);
	
	bufferCurrentOutput += (dividerResistorCurrentOutput * referenceVoltageForAdc * (ADC3->JDR2) - offsetForOutputCurrentSensor) / SensitivityCurrentSensor;
	bufferCurrentInput += (dividerResistorCurrentInput * referenceVoltageForAdc * (ADC3->JDR3) - offsetForInputCurrentSensor) / SensitivityCurrentSensor;
	bufferVoltageInput += deviderResistorVoltageInput * referenceVoltageForAdc * (ADC3->JDR4);

	stepFilterForAdc++;

	if (stepFilterForAdc == allStepFilterAdc) {

		U_Net = resultVoltageOutput = bufferVoltageOutput / allStepFilterAdc; 
		
		resultCurrentOutput = bufferCurrentOutput / allStepFilterAdc;
		resultCurrentInput = bufferCurrentInput / allStepFilterAdc;
		resultVoltageInput = bufferVoltageInput / allStepFilterAdc;

		bufferCurrentOutput = 0;
		bufferVoltageOutput = 0;
		bufferCurrentInput = 0;
		bufferVoltageInput = 0;

		stepFilterForAdc = 0;

	}
	DbPinToggle;
}

/********************************* END OF FILE **********************************/