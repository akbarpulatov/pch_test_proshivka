/********************************************************************************
 * project																		*
 *																				*
 * file			SystemMonitor.c													*
 * author		Akbar Pulatov													*
 * date																			*
 * copyright	Akbar Pulatov(C)												*
 * brief																		*
 *																				*
 ********************************************************************************/

/********************************************************************************
 * Include 
 ********************************************************************************/

#include "SystemMonitor.h"

/********************************************************************************
 * Used variable
 ********************************************************************************/



/********************************************************************************
 * Initialization for Debug 
 * GPIO - PG7
 ********************************************************************************/
void InitGpioForDebug(void)
{
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOGEN;
	
	//PG7
	GPIOG->MODER &= ~GPIO_MODER_MODE7;
	GPIOG->OTYPER &= ~GPIO_OTYPER_OT7;
	GPIOG->OSPEEDR &= ~GPIO_OSPEEDR_OSPEED7;
		
	GPIOG->MODER |= 0b01 << GPIO_MODER_MODE7_Pos;
	GPIOG->OTYPER |= 0b00 << GPIO_OTYPER_OT7_Pos;
	GPIOG->OSPEEDR |= 0b11 << GPIO_OSPEEDR_OSPEED7_Pos;
}

/********************************* END OF FILE **********************************/